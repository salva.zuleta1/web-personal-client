import React from 'react'
import LogoWhite from '../../../../assets/img/png/logo-white.png'
import SocialLinks from '../../SocialLinks'

import './MyInfo.scss'
export default function MyInfo() {
    return (
        <div className="my-info">
           <img src={LogoWhite} alt="Salvador Zuleta" />
           <h4>Entra en el mundo del desarrollo web, disfruta creando proyectos de todo tipo, deja que tu imaginacion te lleve a nuevas fronteras</h4>
           <SocialLinks />
        </div>
    )
}
