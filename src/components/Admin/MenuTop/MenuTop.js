import React from 'react'
import { Button } from "antd";
import { PoweroffOutlined, MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import AgusLogo from '../../../assets/img/svg/Syllab-logo-2.svg';
import { logout } from '../../../api/auth';
import '../../../assets/styles/components/MenuTop.scss';




const MenuTop = (props) => {
    const { menuCollapsed, setMenuCollapsed } = props;

    const logoutUser = () => {
        logout();
        window.location.reload();
    }

    return (
        <div className="menu-top">
            <div className="menu-top__left">
                <img className="menu-top__left-logo"
                    src={AgusLogo}
                    alt="Salvador Zuleta"
                />
                <Button type="link" onClick={() => setMenuCollapsed(!menuCollapsed)}>
                    {menuCollapsed ? <MenuFoldOutlined /> : <MenuUnfoldOutlined />}
                </Button>
            </div>
            <div className="menu-top__right">
                <Button type="link" onClick={logoutUser}>
                    <PoweroffOutlined />
                </Button>
            </div>
        </div>
    )
}


export default MenuTop
